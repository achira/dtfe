import React from 'react';
import Head from 'next/head';
import StaffLayout from '@/components/layouts/StaffLayout';
import StaffList from '@/components/layouts/StaffList';

const staffs = () => {
  return (
    <div>
      <Head>
        <title>test</title>
      </Head>
      <StaffLayout>
        <StaffList />
      </StaffLayout>
    </div>
  );
};

export default staffs;
