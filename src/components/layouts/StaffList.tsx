import React, { useState } from 'react';
import { Table, Breadcrumb, Button } from 'antd';
import SearchStaff from '../SearchStaff';

const columns = [
  {
    title: 'Status',
    dataIndex: 'status',
  },
  {
    title: 'Name',
    dataIndex: 'name',
  },
  {
    title: 'User',
    dataIndex: 'user',
  },
  {
    title: 'Role',
    dataIndex: 'role',
  },
  {
    title: 'Phone',
    dataIndex: 'phone',
  },
];

const data = [];
let i;
for (i = 0; i < 50; i++) {
  data.push({
    key: i,
    status: `Active`,
    name: `Edward King ${i}`,
    user: `chamoi.roi${i}`,
    role: `Customer Service`,
    phone: `09x-xxx-xxxx`,
  });
}

const StaffList = () => {
  return (
    <>
      <div>
        <div className="flex">
          <div className="flex-auto">
            <span>Staff</span>
            <span className="mx-2">&gt;</span>
            <span>Staff management</span>
          </div>
          <div className="flex-auto"></div>
          <div className="flex-auto text-right pb-2">
            <Button className="text-blue-500 mx-2">Export</Button>
            <Button className="text-blue-500">Add</Button>
          </div>
        </div>
        <SearchStaff />
        <Table columns={columns} dataSource={data} />
      </div>
    </>
  );
};

export default StaffList;
