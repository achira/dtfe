import React from 'react';
import { Layout, Menu, Dropdown, Button, Image, Input, DatePicker, Row, Col, Space  } from 'antd';
import {
  BellOutlined,
  LineChartOutlined,
  EllipsisOutlined,
  TeamOutlined,
  UserOutlined,
  FileImageOutlined,
} from '@ant-design/icons';
import Link from 'next/link';

const menu = (
  <Menu style={{ width: 256 }}>
    <Menu.Item>
      <div>
        <TeamOutlined className="mx-2" />
        Staff
      </div>
    </Menu.Item>
    <Menu.Item>2nd menu item</Menu.Item>
    <Menu.Item>3rd menu item</Menu.Item>
  </Menu>
);

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

interface Props {}

const StaffLayout = ({ children }) => {
  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 24 },
  };
  return (
    <>
      <div className="w-full">
        <Layout >
          <Header
            className="site-layout-background bg-gray-200 pb-16"
            style={{ height: '50px' }}
          >
            <div className="flex text-l">
              <div className="flex-auto font-bold">
                <span>
                  <FileImageOutlined className="pr-2" />
                  Logo
                </span>
              </div>
              <div className="flex-auto">
                <span>
                  <Button type="text" className="hover:text-blue-500 ">
                    Customer Service
                  </Button>
                </span>
                <span className="pl-12">
                  <Button type="text" className="hover:text-blue-500 ">
                    Marketing
                  </Button>
                </span>
              </div>
              <div className="flex-auto text-right justify-items-center">
                <span className="pl-80 text-blue-500">
                  <Button
                    className="mx-1"
                    type="text"
                    shape="circle"
                    size="large"
                    icon={<BellOutlined />}
                  />
                  <button className="bg-gray-200 text-black focus:outline-none  hover:text-blue-500 ">
                    <Button
                      className="text-white mx-1 bg-gray-300 hover:bg-blue-100 hover:text-blue-500"
                      type="text"
                      shape="circle"
                      size="small"
                      icon={<UserOutlined />}
                    />
                    Chamoi
                  </button>
                  <Dropdown overlay={menu} placement="bottomRight" arrow>
                    <Button
                      className="mx-1 hover:text-blue-500 hover:bg-blue-100"
                      type="text"
                      shape="circle"
                      size="large"
                      icon={<EllipsisOutlined />}
                    />
                  </Dropdown>
                </span>
              </div>
            </div>
          </Header>
        </Layout>
        <Layout>
          <Sider className="bg-gray-200" width="256px">
            <Menu className="bg-gray-200" mode="inline">
              <Menu.Item key="1" icon={<LineChartOutlined />}>
                Dashboard
              </Menu.Item>
              <SubMenu key="sub1" icon={<UserOutlined />} title="Staff">
                <Menu.Item key="3">Staff management</Menu.Item>
                <Menu.Item key="4">Role managent</Menu.Item>
              </SubMenu>
              <SubMenu key="sub2" icon={<TeamOutlined />} title="Member">
                <Menu.Item key="5">Member management</Menu.Item>
                <Menu.Item key="6">Group management</Menu.Item>
                <Menu.Item key="7">Tags management</Menu.Item>
              </SubMenu>
            </Menu>
          </Sider>
          <Layout style={{ paddingTop: '20px' }}>
            <Content style={{ margin: '0 16px' }}>{children}</Content>
            <Footer style={{ textAlign: 'center' }}>
              ©2021 Created by Daytech
            </Footer>
          </Layout>
        </Layout>
      </div>
    </>
  );
};

export default StaffLayout;
