import React from 'react';
import { Input, DatePicker, Row, Col, Space } from 'antd';
const { RangePicker } = DatePicker;

import { FilterFilled, SearchOutlined } from '@ant-design/icons';

const { Search } = Input;

interface Props {}

const SearchStaff = () => {
  const handleFilter = () => {
    alert('FilterFilled');
  };

  return (
    <>
      <div className="bg-gray-200 p-2">
        <Row gutter={[10, 16]} className="">
          <Col span={17}>
            <Input
              size="large"
              placeholder="search"
              suffix={<SearchOutlined />}
            />
          </Col>
          <Col span={6}>
            <Space direction="vertical" size={12}>
              <RangePicker size="large" />
            </Space>
          </Col>
          <Col span={1} className="text-center pt-2">
            <FilterFilled onClick={handleFilter} />
          </Col>
        </Row>
      </div>
    </>
  );
};

export default SearchStaff;
